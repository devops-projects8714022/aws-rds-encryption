output "rds_instance_ids" {
  description = "IDs of the RDS instances"
  value       = aws_db_instance.default[*].id
}

output "rds_endpoint_addresses" {
  description = "Endpoint addresses of the RDS instances"
  value       = aws_db_instance.default[*].endpoint
}
