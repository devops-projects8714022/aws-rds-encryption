### Документация к Terraform модулю для создания зашифрованных RDS-инстансов

Этот Terraform модуль предназначен для создания зашифрованных RDS-инстансов для всех производственных баз данных. Модуль поддерживает гибкую настройку и может быть использован для различных типов баз данных.

#### Структура директории

1. **main.tf**: Основной файл модуля, содержащий ресурсы для создания KMS ключа, алиаса для KMS ключа, группы подсетей для RDS и самих RDS-инстансов.

2. **variables.tf**: Файл, определяющий переменные, используемые в модуле. Он содержит все настраиваемые параметры, такие как количество инстансов, тип хранения, версия движка базы данных, параметры для подключения и другие.

3. **outputs.tf**: Файл, определяющий выходные данные модуля, такие как ID созданных RDS-инстансов и их конечные точки.

### Параметры модуля

- **enable_key_rotation**: Включить или отключить ротацию ключей KMS.
- **kms_key_alias**: Алиас для KMS ключа.
- **subnet_group_name**: Название группы подсетей для базы данных.
- **subnet_ids**: Список ID подсетей, которые будут использоваться для группы подсетей базы данных.
- **tags**: Теги, которые будут применяться к ресурсам.
- **instance_count**: Количество создаваемых RDS-инстансов.
- **allocated_storage**: Объем выделенного хранилища для RDS-инстанса (в ГБ).
- **storage_type**: Тип хранилища для RDS-инстанса.
- **engine**: Движок базы данных (например, MySQL, PostgreSQL).
- **engine_version**: Версия движка базы данных.
- **instance_class**: Класс инстанса для RDS.
- **db_name**: Название базы данных.
- **username**: Имя пользователя для базы данных.
- **password**: Пароль для пользователя базы данных.
- **multi_az**: Указать, будет ли RDS-инстанс развернут в нескольких зонах доступности.
- **publicly_accessible**: Указать, будет ли RDS-инстанс доступен публично.
- **skip_final_snapshot**: Пропустить или не пропустить создание финального снимка перед удалением инстанса.

### Выходные данные

- **rds_instance_ids**: ID созданных RDS-инстансов.
- **rds_endpoint_addresses**: Адреса конечных точек созданных RDS-инстансов.

### Использование модуля

Для использования модуля создайте новый Terraform конфигурационный файл, указывающий путь к модулю и необходимые параметры. Затем инициализируйте Terraform, спланируйте развертывание и примените конфигурацию.