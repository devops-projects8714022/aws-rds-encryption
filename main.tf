resource "aws_kms_key" "rds_encryption_key" {
  description         = "KMS key for RDS encryption"
  enable_key_rotation = var.enable_key_rotation
}

resource "aws_kms_alias" "rds_encryption_key_alias" {
  name          = var.kms_key_alias
  target_key_id = aws_kms_key.rds_encryption_key.id
}

resource "aws_db_subnet_group" "default" {
  name       = var.subnet_group_name
  subnet_ids = var.subnet_ids

  tags = var.tags
}

resource "aws_db_instance" "default" {
  count                = var.instance_count
  allocated_storage    = var.allocated_storage
  storage_type         = var.storage_type
  engine               = var.engine
  engine_version       = var.engine_version
  instance_class       = var.instance_class
  name                 = var.db_name
  username             = var.username
  password             = var.password
  db_subnet_group_name = aws_db_subnet_group.default.name
  multi_az             = var.multi_az
  publicly_accessible  = var.publicly_accessible
  storage_encrypted    = true
  kms_key_id           = aws_kms_key.rds_encryption_key.arn
  skip_final_snapshot  = var.skip_final_snapshot

  tags = var.tags
}
